<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>峰凌博客</title>
    <style>
        *{
            margin: 0;
            padding: 0;
        }

        body{
            font-size: 15px;
            color: #444;
        }

        .nav{
            border-bottom: 1px solid #ddd;
            padding: 10px 20px;
            text-align: right;
        }

        .nav a{
            color: #444;
            text-decoration: none;
            margin-left: 20px;
            font-size: 20px;

        }

    </style>
</head>
<body>

<div class="nav">
    <a href="/login">登录</a>
    <a href="/register">注册</a>
</div>

<div class="footer">
    <h1>下午好${login_user.nickname}</h1>
    <p>当前注册用户总数：${userTotal}</p>
</div>

</body>
</html>
