<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>系统错误</title>
    <style>
        body{
            text-align: center;
        }
    </style>
</head>
<body>

<h1>${title}</h1>
<p>${msg}</p>

<div>
    <a href="javascript:history.back()">后退</a>
    <a href="/index">返回首页</a>

    <%--<button type="button" onclick="goBack()">后退</button>
    <button type="button" onclick="goIndex()">返回首页</button>--%>
</div>
</body>
<script type="text/javascript">

    function goBack() {
        // 等价于点击浏览器的后退按钮
        history.back();
    }

    function goIndex() {
        // 等价于Servlet中重定向
        location.href = "/index";
    }

</script>
</html>
