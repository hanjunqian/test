package com.stx.blog.test;

import com.stx.fl.core.DbTools;
import com.stx.fl.core.Entity;
import org.junit.Test;

import java.sql.*;

public class DbTest {

    @Test
    public void testQuery(){
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try{
            conn = DbTools.getConnection();
            String sql = "SELECT * FROM user";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()){
                System.out.println(rs.getString("username"));
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            DbTools.close(rs);
            DbTools.close(stmt);
            DbTools.close(conn);
        }



    }


}
