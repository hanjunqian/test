package com.stx.fl.blog.entity;

import com.stx.fl.core.Entity;
import lombok.Data;

import java.util.Date;

/**
 * 文章评论实体
 */
@Data
public class Comment extends Entity {
    private User user;

    private Article article;

    private String content;

    private Date addTime;

    private Integer status;
}
