package com.stx.fl.blog.dao;

import com.stx.fl.blog.entity.Article;
import com.stx.fl.blog.entity.User;
import com.stx.fl.core.DbTools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ArticleDao {
    public void insert(String title, String summary, String content, User user) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = DbTools.getConnection();
            String sql = "insert into article(user_id, title, summary, content, add_time) values(?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, user.getId());
            stmt.setString(2, title);
            stmt.setString(3, summary);
            stmt.setString(4, content);

            Timestamp time = new Timestamp(System.currentTimeMillis());
            stmt.setTimestamp(5, time);

            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbTools.close(rs);
            DbTools.close(stmt);
            DbTools.close(conn);
        }
    }

    public List<Article> selctAll() {
        List<Article> articleList = new ArrayList<>();

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = DbTools.getConnection();
            String sql = "select * from article";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            Article a;
            while (rs.next()){
                a = new Article();
                a.setId(rs.getInt("id"));
                a.setTitle(rs.getString("title"));
                a.setSummary(rs.getString("summary"));

                Timestamp time = rs.getTimestamp("add_time");
                Date addTime = new Date(time.getTime());
                a.setAddTime(addTime);

                // 把当前记录对象，添加到列表
                articleList.add(a);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbTools.close(rs);
            DbTools.close(stmt);
            DbTools.close(conn);
        }

        return articleList;
    }

    public Article selectById(String id) {
        Article article = null;

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = DbTools.getConnection();
            //1.查询文章对象

            String sql = "select a.*,u.nickname,u.avatar from article a, user u  where a.user id = u.id and a.id=? ";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1,id);
            rs = stmt.executeQuery();
            while (rs.next()){
                article = new Article();
                article = new Article();
                article.setId(rs.getInt("id"));
                article.setTitle(rs.getString("title"));
                article.setSummary(rs.getString("summary"));
                article.setContent(rs.getString("content"));
                article.setReadTotal(rs.getInt("read_total"));

                Timestamp time = rs.getTimestamp("add_time");
                Date addTime = new Date(time.getTime());
                article.setAddTime(addTime);

                User user = new User();
                user.setId(rs.getInt("user_id"));
                user.setNickname(rs.getString("nickname"));
                user.setAvatar(rs.getString("avatar"));

                article.setUser(user);

            }

            //2.增加文章阅读技术
            //提前关闭第一步使用的stmt
            stmt.close();

            sql= "update article set read_total = read_total + 1 where id = ?";
            stmt = conn.prepareStatement(sql);

            stmt.setString(1,id);
            stmt.executeUpdate();




        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbTools.close(rs);
            DbTools.close(stmt);
            DbTools.close(conn);
        }



        return article;




    }
}
