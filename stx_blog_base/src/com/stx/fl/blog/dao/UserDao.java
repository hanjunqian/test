package com.stx.fl.blog.dao;

import com.stx.fl.blog.entity.User;
import com.stx.fl.core.DbTools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;

public class UserDao {

    public void insert(String username, String password, String nickname) {
        String sql = "insert into user(username, password, nickname) values(?, ?, ?)";

        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = DbTools.getConnection();
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            stmt.setString(2, password);
            stmt.setString(3, nickname);

            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbTools.close(stmt);
            DbTools.close(conn);
        }


    }

    /**
     * 根据用户查询一条用户记录
     * @param username
     * @return
     */
    public User selectByUsername(String username) {
        User user = null;

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = DbTools.getConnection();
            String sql = "select * from user where username = ? LIMIT 1";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);

            rs = stmt.executeQuery();
            while (rs.next()){
                user = new User();
                user.setId(rs.getInt("id"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setNickname(rs.getString("nickname"));
                user.setAvatar(rs.getString("avatar"));
                user.setStatus(rs.getInt("status"));

                // java.sql.Date
                // java.util.Date
                // 他们不一样
                Timestamp time = rs.getTimestamp("add_time");
                Date addTime = new Date(time.getTime());

                user.setAddTime(addTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbTools.close(rs);
            DbTools.close(stmt);
            DbTools.close(conn);
        }

        return user;
    }
}
