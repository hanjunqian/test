package com.stx.fl.blog.servlet;

import com.stx.fl.blog.dao.UserDao;
import com.stx.fl.blog.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    UserDao userDao = new UserDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher("/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        boolean ok = false; //是否登录成功
        String msg = null; // 登录失败的原因

        // 1.应该检查参数是否为空
        // 2.检查用户名是否存在

        User user = userDao.selectByUsername(username);
        if(user == null){
            msg = "用户名不存在";
        }else{
            String dbPassword = user.getPassword();
            if(dbPassword.equals(password)){
                ok = true;
            }else{
                msg = "密码错误";
            }
        }

        // 跳转到登录结果页面
        if(ok){
            // 需要添加会话保持
            HttpSession session = req.getSession();
            req.getSession().setAttribute("login_user",user);
            resp.sendRedirect("/index");
        }else{
            req.setAttribute("title", "登录失败");
            req.setAttribute("msg", msg);
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }

    }
}
