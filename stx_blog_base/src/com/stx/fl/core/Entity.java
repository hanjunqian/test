package com.stx.fl.core;

/**
 * 包含主键ID的实体类
 */
public abstract class Entity {

    protected Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}


