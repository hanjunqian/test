package com.stx.fl.core;

import java.sql.*;

/**
 * 数据库工具类
 */
public class DbTools {
    private static String dbUser = "root";
    private static String dbPassword = "123456";
    private static String url = "jdbc:mysql://127.0.0.1:3307/stx_blog?useSSL=false";

    /**
     * 通过静态代码块，加载数据库驱动
     */
    static{
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取数据库链接
     * @return
     */
    public static Connection getConnection(){
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url, dbUser, dbPassword);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }


    public static int queryForInt(String sql, String... params){
        int result = 0;

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(sql);

            for (int i = 0; i < params.length; i++) {
                stmt.setString(i + 1, params[i]);
            }

            rs = stmt.executeQuery();
            while (rs.next()){
                result = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(rs);
            close(stmt);
            close(conn);
        }

        return result;
    }



    /**
     * 关闭结果集
     * @param rs
     */
    public static void close(ResultSet rs){
        if(rs != null){
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭操作语句
     * @param stmt
     */
    public static void close(Statement stmt){
        if(stmt != null){
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭链接
     * @param conn
     */
    public static void close(Connection conn){
        if(conn != null){
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
